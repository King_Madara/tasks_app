﻿namespace My_Tasks
{
    partial class AddAreaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnMinimizar = new Guna.UI.WinForms.GunaControlBox();
            this.btnMaximizar = new Guna.UI.WinForms.GunaControlBox();
            this.gEaddArea = new Guna.UI.WinForms.GunaElipse(this.components);
            this.btnBack = new Guna.UI.WinForms.GunaButton();
            this.txtboxNombre = new Guna.UI.WinForms.GunaTextBox();
            this.lblNombre = new Guna.UI.WinForms.GunaLabel();
            this.lblTitulo = new Guna.UI.WinForms.GunaLabel();
            this.btnOK = new Guna.UI.WinForms.GunaLabel();
            this.SuspendLayout();
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMinimizar.AnimationHoverSpeed = 0.07F;
            this.btnMinimizar.AnimationSpeed = 0.03F;
            this.btnMinimizar.BackColor = System.Drawing.Color.Transparent;
            this.btnMinimizar.ControlBoxType = Guna.UI.WinForms.FormControlBoxType.MinimizeBox;
            this.btnMinimizar.IconColor = System.Drawing.Color.White;
            this.btnMinimizar.IconSize = 15F;
            this.btnMinimizar.Location = new System.Drawing.Point(472, 0);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.OnHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(137)))), ((int)(((byte)(131)))));
            this.btnMinimizar.OnHoverIconColor = System.Drawing.Color.White;
            this.btnMinimizar.OnPressedColor = System.Drawing.Color.Black;
            this.btnMinimizar.Size = new System.Drawing.Size(45, 25);
            this.btnMinimizar.TabIndex = 25;
            // 
            // btnMaximizar
            // 
            this.btnMaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMaximizar.AnimationHoverSpeed = 0.07F;
            this.btnMaximizar.AnimationSpeed = 0.03F;
            this.btnMaximizar.BackColor = System.Drawing.Color.Transparent;
            this.btnMaximizar.ControlBoxType = Guna.UI.WinForms.FormControlBoxType.MaximizeBox;
            this.btnMaximizar.IconColor = System.Drawing.Color.White;
            this.btnMaximizar.IconSize = 15F;
            this.btnMaximizar.Location = new System.Drawing.Point(517, 0);
            this.btnMaximizar.Name = "btnMaximizar";
            this.btnMaximizar.OnHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(137)))), ((int)(((byte)(131)))));
            this.btnMaximizar.OnHoverIconColor = System.Drawing.Color.White;
            this.btnMaximizar.OnPressedColor = System.Drawing.Color.Black;
            this.btnMaximizar.Size = new System.Drawing.Size(45, 25);
            this.btnMaximizar.TabIndex = 24;
            // 
            // gEaddArea
            // 
            this.gEaddArea.Radius = 7;
            this.gEaddArea.TargetControl = this;
            // 
            // btnBack
            // 
            this.btnBack.AnimationHoverSpeed = 0.07F;
            this.btnBack.AnimationSpeed = 0.03F;
            this.btnBack.BackColor = System.Drawing.Color.Transparent;
            this.btnBack.BaseColor = System.Drawing.Color.Transparent;
            this.btnBack.BorderColor = System.Drawing.Color.Black;
            this.btnBack.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnBack.FocusedColor = System.Drawing.Color.Empty;
            this.btnBack.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Image = global::My_Tasks.Properties.Resources.White_BackIcon;
            this.btnBack.ImageAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.btnBack.ImageSize = new System.Drawing.Size(20, 20);
            this.btnBack.Location = new System.Drawing.Point(-1, 0);
            this.btnBack.Name = "btnBack";
            this.btnBack.OnHoverBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(151)))), ((int)(((byte)(143)))), ((int)(((byte)(255)))));
            this.btnBack.OnHoverBorderColor = System.Drawing.Color.Black;
            this.btnBack.OnHoverForeColor = System.Drawing.Color.White;
            this.btnBack.OnHoverImage = null;
            this.btnBack.OnPressedColor = System.Drawing.Color.Black;
            this.btnBack.Size = new System.Drawing.Size(50, 37);
            this.btnBack.TabIndex = 26;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // txtboxNombre
            // 
            this.txtboxNombre.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(221)))), ((int)(((byte)(196)))));
            this.txtboxNombre.BorderColor = System.Drawing.Color.Silver;
            this.txtboxNombre.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtboxNombre.FocusedBaseColor = System.Drawing.Color.White;
            this.txtboxNombre.FocusedBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            this.txtboxNombre.FocusedForeColor = System.Drawing.SystemColors.ControlText;
            this.txtboxNombre.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtboxNombre.Location = new System.Drawing.Point(218, 146);
            this.txtboxNombre.Name = "txtboxNombre";
            this.txtboxNombre.PasswordChar = '\0';
            this.txtboxNombre.SelectedText = "";
            this.txtboxNombre.Size = new System.Drawing.Size(299, 25);
            this.txtboxNombre.TabIndex = 30;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Times New Roman", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(221)))), ((int)(((byte)(196)))));
            this.lblNombre.Location = new System.Drawing.Point(58, 130);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(131, 40);
            this.lblNombre.TabIndex = 29;
            this.lblNombre.Text = "Nombre";
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(221)))), ((int)(((byte)(196)))));
            this.lblTitulo.Location = new System.Drawing.Point(179, 41);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(207, 55);
            this.lblTitulo.TabIndex = 31;
            this.lblTitulo.Text = "Add Area";
            // 
            // btnOK
            // 
            this.btnOK.AutoSize = true;
            this.btnOK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btnOK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOK.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(221)))), ((int)(((byte)(196)))));
            this.btnOK.Location = new System.Drawing.Point(227, 219);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(96, 57);
            this.btnOK.TabIndex = 32;
            this.btnOK.Text = "OK";
            // 
            // AddAreaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(29)))), ((int)(((byte)(49)))));
            this.ClientSize = new System.Drawing.Size(563, 299);
            this.ControlBox = false;
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.txtboxNombre);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnMinimizar);
            this.Controls.Add(this.btnMaximizar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddAreaForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddAreaForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Guna.UI.WinForms.GunaControlBox btnMinimizar;
        private Guna.UI.WinForms.GunaControlBox btnMaximizar;
        private Guna.UI.WinForms.GunaElipse gEaddArea;
        private Guna.UI.WinForms.GunaButton btnBack;
        private Guna.UI.WinForms.GunaTextBox txtboxNombre;
        private Guna.UI.WinForms.GunaLabel lblNombre;
        private Guna.UI.WinForms.GunaLabel btnOK;
        private Guna.UI.WinForms.GunaLabel lblTitulo;
    }
}