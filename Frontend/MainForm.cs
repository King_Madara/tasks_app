﻿using System;
using System.Windows.Forms;

namespace My_Tasks
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            InicializarFormulario();
        }

        public void InicializarFormulario(){
            CurrentDay();
        }

        #region Eventos

        private void btnAddTask_Click(object sender, EventArgs e)
        {
            AddTaskForm addTaskForm = new AddTaskForm();
            addTaskForm.ShowDialog();

        }

        #endregion

        #region Métodos Privados

        private void CurrentDay(){
            DateTime currentday = DateTime.Now;
            lblFechaActual.Text = currentday.ToLongDateString();
        }

        #endregion
    }
}
