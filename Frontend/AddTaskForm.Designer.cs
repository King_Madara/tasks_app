﻿namespace My_Tasks
{
    partial class AddTaskForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnMinimizar = new Guna.UI.WinForms.GunaControlBox();
            this.btnMaximizar = new Guna.UI.WinForms.GunaControlBox();
            this.gEaddForm = new Guna.UI.WinForms.GunaElipse(this.components);
            this.combNombre = new Guna.UI.WinForms.GunaComboBox();
            this.lblNombre = new Guna.UI.WinForms.GunaLabel();
            this.combArea = new Guna.UI.WinForms.GunaComboBox();
            this.lblArea = new Guna.UI.WinForms.GunaLabel();
            this.btnCerrar = new Guna.UI.WinForms.GunaControlBox();
            this.lblTitulo = new Guna.UI.WinForms.GunaLabel();
            this.btnOK = new Guna.UI.WinForms.GunaLabel();
            this.SuspendLayout();
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMinimizar.AnimationHoverSpeed = 0.07F;
            this.btnMinimizar.AnimationSpeed = 0.03F;
            this.btnMinimizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(29)))), ((int)(((byte)(49)))));
            this.btnMinimizar.ControlBoxType = Guna.UI.WinForms.FormControlBoxType.MinimizeBox;
            this.btnMinimizar.IconColor = System.Drawing.Color.White;
            this.btnMinimizar.IconSize = 15F;
            this.btnMinimizar.Location = new System.Drawing.Point(430, 0);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.OnHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(137)))), ((int)(((byte)(131)))));
            this.btnMinimizar.OnHoverIconColor = System.Drawing.Color.White;
            this.btnMinimizar.OnPressedColor = System.Drawing.Color.Black;
            this.btnMinimizar.Size = new System.Drawing.Size(45, 25);
            this.btnMinimizar.TabIndex = 13;
            // 
            // btnMaximizar
            // 
            this.btnMaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMaximizar.AnimationHoverSpeed = 0.07F;
            this.btnMaximizar.AnimationSpeed = 0.03F;
            this.btnMaximizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(29)))), ((int)(((byte)(49)))));
            this.btnMaximizar.ControlBoxType = Guna.UI.WinForms.FormControlBoxType.MaximizeBox;
            this.btnMaximizar.IconColor = System.Drawing.Color.White;
            this.btnMaximizar.IconSize = 15F;
            this.btnMaximizar.Location = new System.Drawing.Point(475, 0);
            this.btnMaximizar.Name = "btnMaximizar";
            this.btnMaximizar.OnHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(137)))), ((int)(((byte)(131)))));
            this.btnMaximizar.OnHoverIconColor = System.Drawing.Color.White;
            this.btnMaximizar.OnPressedColor = System.Drawing.Color.Black;
            this.btnMaximizar.Size = new System.Drawing.Size(45, 25);
            this.btnMaximizar.TabIndex = 12;
            // 
            // gEaddForm
            // 
            this.gEaddForm.Radius = 7;
            this.gEaddForm.TargetControl = this;
            // 
            // combNombre
            // 
            this.combNombre.BackColor = System.Drawing.Color.Transparent;
            this.combNombre.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(221)))), ((int)(((byte)(196)))));
            this.combNombre.BorderColor = System.Drawing.Color.Silver;
            this.combNombre.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.combNombre.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combNombre.FocusedColor = System.Drawing.Color.Empty;
            this.combNombre.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combNombre.ForeColor = System.Drawing.Color.Black;
            this.combNombre.FormattingEnabled = true;
            this.combNombre.Location = new System.Drawing.Point(203, 130);
            this.combNombre.Name = "combNombre";
            this.combNombre.OnHoverItemBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            this.combNombre.OnHoverItemForeColor = System.Drawing.Color.White;
            this.combNombre.Size = new System.Drawing.Size(287, 26);
            this.combNombre.TabIndex = 14;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.BackColor = System.Drawing.Color.Transparent;
            this.lblNombre.Font = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(221)))), ((int)(((byte)(196)))));
            this.lblNombre.Location = new System.Drawing.Point(48, 125);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(118, 36);
            this.lblNombre.TabIndex = 16;
            this.lblNombre.Text = "Nombre";
            // 
            // combArea
            // 
            this.combArea.BackColor = System.Drawing.Color.Transparent;
            this.combArea.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(221)))), ((int)(((byte)(196)))));
            this.combArea.BorderColor = System.Drawing.Color.Silver;
            this.combArea.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.combArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combArea.FocusedColor = System.Drawing.Color.Empty;
            this.combArea.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combArea.ForeColor = System.Drawing.Color.Black;
            this.combArea.FormattingEnabled = true;
            this.combArea.Location = new System.Drawing.Point(203, 177);
            this.combArea.Name = "combArea";
            this.combArea.OnHoverItemBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(88)))), ((int)(((byte)(255)))));
            this.combArea.OnHoverItemForeColor = System.Drawing.Color.White;
            this.combArea.Size = new System.Drawing.Size(287, 26);
            this.combArea.TabIndex = 17;
            // 
            // lblArea
            // 
            this.lblArea.AutoSize = true;
            this.lblArea.BackColor = System.Drawing.Color.Transparent;
            this.lblArea.Font = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArea.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(221)))), ((int)(((byte)(196)))));
            this.lblArea.Location = new System.Drawing.Point(48, 167);
            this.lblArea.Name = "lblArea";
            this.lblArea.Size = new System.Drawing.Size(76, 36);
            this.lblArea.TabIndex = 18;
            this.lblArea.Text = "Area";
            // 
            // btnCerrar
            // 
            this.btnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrar.AnimationHoverSpeed = 0.07F;
            this.btnCerrar.AnimationSpeed = 0.03F;
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(29)))), ((int)(((byte)(49)))));
            this.btnCerrar.IconColor = System.Drawing.Color.White;
            this.btnCerrar.IconSize = 15F;
            this.btnCerrar.Location = new System.Drawing.Point(519, 0);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.OnHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(137)))), ((int)(((byte)(131)))));
            this.btnCerrar.OnHoverIconColor = System.Drawing.Color.Black;
            this.btnCerrar.OnPressedColor = System.Drawing.Color.Black;
            this.btnCerrar.Size = new System.Drawing.Size(45, 25);
            this.btnCerrar.TabIndex = 11;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(221)))), ((int)(((byte)(196)))));
            this.lblTitulo.Location = new System.Drawing.Point(183, 47);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(206, 55);
            this.lblTitulo.TabIndex = 32;
            this.lblTitulo.Text = "Add Task";
            // 
            // btnOK
            // 
            this.btnOK.AutoSize = true;
            this.btnOK.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btnOK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOK.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(221)))), ((int)(((byte)(196)))));
            this.btnOK.Location = new System.Drawing.Point(237, 246);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(96, 57);
            this.btnOK.TabIndex = 33;
            this.btnOK.Text = "OK";
            // 
            // AddTaskForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(29)))), ((int)(((byte)(49)))));
            this.ClientSize = new System.Drawing.Size(565, 344);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.lblArea);
            this.Controls.Add(this.combArea);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.combNombre);
            this.Controls.Add(this.btnMinimizar);
            this.Controls.Add(this.btnMaximizar);
            this.Controls.Add(this.btnCerrar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddTaskForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddTaskForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI.WinForms.GunaControlBox btnMinimizar;
        private Guna.UI.WinForms.GunaControlBox btnMaximizar;
        private Guna.UI.WinForms.GunaElipse gEaddForm;
        private Guna.UI.WinForms.GunaComboBox combNombre;
        private Guna.UI.WinForms.GunaLabel lblNombre;
        private Guna.UI.WinForms.GunaLabel lblArea;
        private Guna.UI.WinForms.GunaComboBox combArea;
        private Guna.UI.WinForms.GunaControlBox btnCerrar;
        private Guna.UI.WinForms.GunaLabel lblTitulo;
        private Guna.UI.WinForms.GunaLabel btnOK;
    }
}