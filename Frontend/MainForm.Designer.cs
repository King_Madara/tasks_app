﻿namespace My_Tasks
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MainFormElipse = new Guna.UI.WinForms.GunaElipse(this.components);
            this.MainPanel = new Guna.UI.WinForms.GunaPanel();
            this.btnSettings = new Guna.UI.WinForms.GunaButton();
            this.btnTasks = new Guna.UI.WinForms.GunaButton();
            this.btnMetrics = new Guna.UI.WinForms.GunaButton();
            this.gunaPanelBelowPerfil = new Guna.UI.WinForms.GunaPanel();
            this.pbFotoPerfilUsuario = new Guna.UI.WinForms.GunaPictureBox();
            this.btnHome = new Guna.UI.WinForms.GunaButton();
            this.contentPanel = new Guna.UI.WinForms.GunaPanel();
            this.gunaLabel2 = new Guna.UI.WinForms.GunaLabel();
            this.gunaLabel1 = new Guna.UI.WinForms.GunaLabel();
            this.btnAddTask = new Guna.UI.WinForms.GunaButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Area = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Actividad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaAsignado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comentario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Realizado = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Modificar = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Eliminar = new System.Windows.Forms.DataGridViewLinkColumn();
            this.btnMinimizar = new Guna.UI.WinForms.GunaControlBox();
            this.lblFechaActual = new Guna.UI.WinForms.GunaLabel();
            this.btnMaximizar = new Guna.UI.WinForms.GunaControlBox();
            this.btnCerrar = new Guna.UI.WinForms.GunaControlBox();
            this.MainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFotoPerfilUsuario)).BeginInit();
            this.contentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // MainFormElipse
            // 
            this.MainFormElipse.Radius = 7;
            this.MainFormElipse.TargetControl = this;
            // 
            // MainPanel
            // 
            this.MainPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(29)))), ((int)(((byte)(49)))));
            this.MainPanel.Controls.Add(this.btnSettings);
            this.MainPanel.Controls.Add(this.btnTasks);
            this.MainPanel.Controls.Add(this.btnMetrics);
            this.MainPanel.Controls.Add(this.gunaPanelBelowPerfil);
            this.MainPanel.Controls.Add(this.pbFotoPerfilUsuario);
            this.MainPanel.Controls.Add(this.btnHome);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(239, 518);
            this.MainPanel.TabIndex = 0;
            // 
            // btnSettings
            // 
            this.btnSettings.AnimationHoverSpeed = 0.07F;
            this.btnSettings.AnimationSpeed = 0.03F;
            this.btnSettings.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(137)))), ((int)(((byte)(131)))));
            this.btnSettings.BorderColor = System.Drawing.Color.Black;
            this.btnSettings.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnSettings.FocusedColor = System.Drawing.Color.Empty;
            this.btnSettings.Font = new System.Drawing.Font("Times New Roman", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSettings.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(233)))), ((int)(((byte)(210)))));
            this.btnSettings.Image = global::My_Tasks.Properties.Resources.SettingsIcon;
            this.btnSettings.ImageAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.btnSettings.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSettings.Location = new System.Drawing.Point(0, 435);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.OnHoverBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(221)))), ((int)(((byte)(196)))));
            this.btnSettings.OnHoverBorderColor = System.Drawing.Color.Black;
            this.btnSettings.OnHoverForeColor = System.Drawing.Color.Black;
            this.btnSettings.OnHoverImage = null;
            this.btnSettings.OnPressedColor = System.Drawing.Color.Black;
            this.btnSettings.Size = new System.Drawing.Size(239, 84);
            this.btnSettings.TabIndex = 4;
            this.btnSettings.Text = "Settings";
            this.btnSettings.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnTasks
            // 
            this.btnTasks.AnimationHoverSpeed = 0.07F;
            this.btnTasks.AnimationSpeed = 0.03F;
            this.btnTasks.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(137)))), ((int)(((byte)(131)))));
            this.btnTasks.BorderColor = System.Drawing.Color.Black;
            this.btnTasks.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnTasks.FocusedColor = System.Drawing.Color.Empty;
            this.btnTasks.Font = new System.Drawing.Font("Times New Roman", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTasks.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(233)))), ((int)(((byte)(210)))));
            this.btnTasks.Image = global::My_Tasks.Properties.Resources.CheckListIcon;
            this.btnTasks.ImageAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.btnTasks.ImageSize = new System.Drawing.Size(20, 20);
            this.btnTasks.Location = new System.Drawing.Point(0, 255);
            this.btnTasks.Name = "btnTasks";
            this.btnTasks.OnHoverBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(221)))), ((int)(((byte)(196)))));
            this.btnTasks.OnHoverBorderColor = System.Drawing.Color.Black;
            this.btnTasks.OnHoverForeColor = System.Drawing.Color.Black;
            this.btnTasks.OnHoverImage = null;
            this.btnTasks.OnPressedColor = System.Drawing.Color.Black;
            this.btnTasks.Size = new System.Drawing.Size(239, 84);
            this.btnTasks.TabIndex = 5;
            this.btnTasks.Text = "Tasks";
            this.btnTasks.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnMetrics
            // 
            this.btnMetrics.AnimationHoverSpeed = 0.07F;
            this.btnMetrics.AnimationSpeed = 0.03F;
            this.btnMetrics.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(137)))), ((int)(((byte)(131)))));
            this.btnMetrics.BorderColor = System.Drawing.Color.Black;
            this.btnMetrics.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnMetrics.FocusedColor = System.Drawing.Color.Empty;
            this.btnMetrics.Font = new System.Drawing.Font("Times New Roman", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMetrics.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(233)))), ((int)(((byte)(210)))));
            this.btnMetrics.Image = global::My_Tasks.Properties.Resources.MetricIcon;
            this.btnMetrics.ImageAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.btnMetrics.ImageSize = new System.Drawing.Size(20, 20);
            this.btnMetrics.Location = new System.Drawing.Point(0, 345);
            this.btnMetrics.Name = "btnMetrics";
            this.btnMetrics.OnHoverBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(221)))), ((int)(((byte)(196)))));
            this.btnMetrics.OnHoverBorderColor = System.Drawing.Color.Black;
            this.btnMetrics.OnHoverForeColor = System.Drawing.Color.Black;
            this.btnMetrics.OnHoverImage = null;
            this.btnMetrics.OnPressedColor = System.Drawing.Color.Black;
            this.btnMetrics.Size = new System.Drawing.Size(239, 84);
            this.btnMetrics.TabIndex = 3;
            this.btnMetrics.Text = "Metrics";
            this.btnMetrics.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // gunaPanelBelowPerfil
            // 
            this.gunaPanelBelowPerfil.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(233)))), ((int)(((byte)(210)))));
            this.gunaPanelBelowPerfil.Location = new System.Drawing.Point(0, 136);
            this.gunaPanelBelowPerfil.Name = "gunaPanelBelowPerfil";
            this.gunaPanelBelowPerfil.Size = new System.Drawing.Size(239, 10);
            this.gunaPanelBelowPerfil.TabIndex = 1;
            // 
            // pbFotoPerfilUsuario
            // 
            this.pbFotoPerfilUsuario.BackColor = System.Drawing.Color.Transparent;
            this.pbFotoPerfilUsuario.BaseColor = System.Drawing.Color.White;
            this.pbFotoPerfilUsuario.Image = global::My_Tasks.Properties.Resources.Kuribo;
            this.pbFotoPerfilUsuario.Location = new System.Drawing.Point(66, 22);
            this.pbFotoPerfilUsuario.Name = "pbFotoPerfilUsuario";
            this.pbFotoPerfilUsuario.Size = new System.Drawing.Size(108, 108);
            this.pbFotoPerfilUsuario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFotoPerfilUsuario.TabIndex = 0;
            this.pbFotoPerfilUsuario.TabStop = false;
            // 
            // btnHome
            // 
            this.btnHome.AnimationHoverSpeed = 0.07F;
            this.btnHome.AnimationSpeed = 0.03F;
            this.btnHome.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(137)))), ((int)(((byte)(131)))));
            this.btnHome.BorderColor = System.Drawing.Color.Black;
            this.btnHome.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnHome.FocusedColor = System.Drawing.Color.Empty;
            this.btnHome.Font = new System.Drawing.Font("Times New Roman", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(233)))), ((int)(((byte)(210)))));
            this.btnHome.Image = global::My_Tasks.Properties.Resources.HomeIcon;
            this.btnHome.ImageAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.btnHome.ImageSize = new System.Drawing.Size(20, 20);
            this.btnHome.Location = new System.Drawing.Point(0, 165);
            this.btnHome.Name = "btnHome";
            this.btnHome.OnHoverBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(221)))), ((int)(((byte)(196)))));
            this.btnHome.OnHoverBorderColor = System.Drawing.Color.Black;
            this.btnHome.OnHoverForeColor = System.Drawing.Color.Black;
            this.btnHome.OnHoverImage = null;
            this.btnHome.OnPressedColor = System.Drawing.Color.Black;
            this.btnHome.Size = new System.Drawing.Size(239, 84);
            this.btnHome.TabIndex = 2;
            this.btnHome.Text = "Home";
            this.btnHome.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // contentPanel
            // 
            this.contentPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(221)))), ((int)(((byte)(196)))));
            this.contentPanel.Controls.Add(this.gunaLabel2);
            this.contentPanel.Controls.Add(this.gunaLabel1);
            this.contentPanel.Controls.Add(this.btnAddTask);
            this.contentPanel.Controls.Add(this.dataGridView1);
            this.contentPanel.Controls.Add(this.btnMinimizar);
            this.contentPanel.Controls.Add(this.lblFechaActual);
            this.contentPanel.Controls.Add(this.btnMaximizar);
            this.contentPanel.Controls.Add(this.btnCerrar);
            this.contentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentPanel.Location = new System.Drawing.Point(239, 0);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(795, 518);
            this.contentPanel.TabIndex = 5;
            // 
            // gunaLabel2
            // 
            this.gunaLabel2.AutoSize = true;
            this.gunaLabel2.Font = new System.Drawing.Font("Times New Roman", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gunaLabel2.Location = new System.Drawing.Point(424, 330);
            this.gunaLabel2.Name = "gunaLabel2";
            this.gunaLabel2.Size = new System.Drawing.Size(246, 31);
            this.gunaLabel2.TabIndex = 16;
            this.gunaLabel2.Text = "No Completados = 5";
            // 
            // gunaLabel1
            // 
            this.gunaLabel1.AutoSize = true;
            this.gunaLabel1.Font = new System.Drawing.Font("Times New Roman", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gunaLabel1.Location = new System.Drawing.Point(145, 330);
            this.gunaLabel1.Name = "gunaLabel1";
            this.gunaLabel1.Size = new System.Drawing.Size(219, 31);
            this.gunaLabel1.TabIndex = 15;
            this.gunaLabel1.Text = "Completados = 10";
            // 
            // btnAddTask
            // 
            this.btnAddTask.AnimationHoverSpeed = 0.07F;
            this.btnAddTask.AnimationSpeed = 0.03F;
            this.btnAddTask.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(29)))), ((int)(((byte)(49)))));
            this.btnAddTask.BorderColor = System.Drawing.Color.Black;
            this.btnAddTask.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnAddTask.FocusedColor = System.Drawing.Color.Empty;
            this.btnAddTask.Font = new System.Drawing.Font("Times New Roman", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddTask.ForeColor = System.Drawing.Color.White;
            this.btnAddTask.Image = ((System.Drawing.Image)(resources.GetObject("btnAddTask.Image")));
            this.btnAddTask.ImageAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.btnAddTask.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddTask.Location = new System.Drawing.Point(32, 92);
            this.btnAddTask.Name = "btnAddTask";
            this.btnAddTask.OnHoverBaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(151)))), ((int)(((byte)(143)))), ((int)(((byte)(255)))));
            this.btnAddTask.OnHoverBorderColor = System.Drawing.Color.Black;
            this.btnAddTask.OnHoverForeColor = System.Drawing.Color.White;
            this.btnAddTask.OnHoverImage = null;
            this.btnAddTask.OnPressedColor = System.Drawing.Color.Black;
            this.btnAddTask.Size = new System.Drawing.Size(743, 38);
            this.btnAddTask.TabIndex = 12;
            this.btnAddTask.Text = "Add Task";
            this.btnAddTask.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.btnAddTask.Click += new System.EventHandler(this.btnAddTask_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Area,
            this.Actividad,
            this.FechaAsignado,
            this.Comentario,
            this.Realizado,
            this.Modificar,
            this.Eliminar});
            this.dataGridView1.Location = new System.Drawing.Point(32, 142);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(743, 169);
            this.dataGridView1.TabIndex = 11;
            // 
            // Area
            // 
            this.Area.HeaderText = "Área";
            this.Area.Name = "Area";
            // 
            // Actividad
            // 
            this.Actividad.HeaderText = "Actividad";
            this.Actividad.Name = "Actividad";
            // 
            // FechaAsignado
            // 
            this.FechaAsignado.HeaderText = "Fecha";
            this.FechaAsignado.Name = "FechaAsignado";
            // 
            // Comentario
            // 
            this.Comentario.HeaderText = "Comentario";
            this.Comentario.Name = "Comentario";
            // 
            // Realizado
            // 
            this.Realizado.HeaderText = "Realizado";
            this.Realizado.Name = "Realizado";
            // 
            // Modificar
            // 
            this.Modificar.HeaderText = "Modificar";
            this.Modificar.Name = "Modificar";
            // 
            // Eliminar
            // 
            this.Eliminar.HeaderText = "Eliminar";
            this.Eliminar.Name = "Eliminar";
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMinimizar.AnimationHoverSpeed = 0.07F;
            this.btnMinimizar.AnimationSpeed = 0.03F;
            this.btnMinimizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(29)))), ((int)(((byte)(49)))));
            this.btnMinimizar.ControlBoxType = Guna.UI.WinForms.FormControlBoxType.MinimizeBox;
            this.btnMinimizar.IconColor = System.Drawing.Color.White;
            this.btnMinimizar.IconSize = 15F;
            this.btnMinimizar.Location = new System.Drawing.Point(660, 0);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.OnHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(137)))), ((int)(((byte)(131)))));
            this.btnMinimizar.OnHoverIconColor = System.Drawing.Color.White;
            this.btnMinimizar.OnPressedColor = System.Drawing.Color.Black;
            this.btnMinimizar.Size = new System.Drawing.Size(45, 25);
            this.btnMinimizar.TabIndex = 10;
            // 
            // lblFechaActual
            // 
            this.lblFechaActual.AutoSize = true;
            this.lblFechaActual.Font = new System.Drawing.Font("Times New Roman", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaActual.Location = new System.Drawing.Point(37, 22);
            this.lblFechaActual.Name = "lblFechaActual";
            this.lblFechaActual.Size = new System.Drawing.Size(157, 31);
            this.lblFechaActual.TabIndex = 6;
            this.lblFechaActual.Text = "Current Day";
            // 
            // btnMaximizar
            // 
            this.btnMaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMaximizar.AnimationHoverSpeed = 0.07F;
            this.btnMaximizar.AnimationSpeed = 0.03F;
            this.btnMaximizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(29)))), ((int)(((byte)(49)))));
            this.btnMaximizar.ControlBoxType = Guna.UI.WinForms.FormControlBoxType.MaximizeBox;
            this.btnMaximizar.IconColor = System.Drawing.Color.White;
            this.btnMaximizar.IconSize = 15F;
            this.btnMaximizar.Location = new System.Drawing.Point(705, 0);
            this.btnMaximizar.Name = "btnMaximizar";
            this.btnMaximizar.OnHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(137)))), ((int)(((byte)(131)))));
            this.btnMaximizar.OnHoverIconColor = System.Drawing.Color.White;
            this.btnMaximizar.OnPressedColor = System.Drawing.Color.Black;
            this.btnMaximizar.Size = new System.Drawing.Size(45, 25);
            this.btnMaximizar.TabIndex = 8;
            // 
            // btnCerrar
            // 
            this.btnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrar.AnimationHoverSpeed = 0.07F;
            this.btnCerrar.AnimationSpeed = 0.03F;
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(29)))), ((int)(((byte)(49)))));
            this.btnCerrar.IconColor = System.Drawing.Color.White;
            this.btnCerrar.IconSize = 15F;
            this.btnCerrar.Location = new System.Drawing.Point(749, 0);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.OnHoverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(137)))), ((int)(((byte)(131)))));
            this.btnCerrar.OnHoverIconColor = System.Drawing.Color.Black;
            this.btnCerrar.OnPressedColor = System.Drawing.Color.Black;
            this.btnCerrar.Size = new System.Drawing.Size(45, 25);
            this.btnCerrar.TabIndex = 7;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1034, 518);
            this.Controls.Add(this.contentPanel);
            this.Controls.Add(this.MainPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.MainPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbFotoPerfilUsuario)).EndInit();
            this.contentPanel.ResumeLayout(false);
            this.contentPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI.WinForms.GunaElipse MainFormElipse;
        private Guna.UI.WinForms.GunaPanel MainPanel;
        private Guna.UI.WinForms.GunaPictureBox pbFotoPerfilUsuario;
        private Guna.UI.WinForms.GunaPanel gunaPanelBelowPerfil;
        private Guna.UI.WinForms.GunaButton btnSettings;
        private Guna.UI.WinForms.GunaButton btnTasks;
        private Guna.UI.WinForms.GunaButton btnMetrics;
        private Guna.UI.WinForms.GunaButton btnHome;
        private Guna.UI.WinForms.GunaPanel contentPanel;
        private Guna.UI.WinForms.GunaLabel lblFechaActual;
        private Guna.UI.WinForms.GunaControlBox btnMaximizar;
        private Guna.UI.WinForms.GunaControlBox btnCerrar;
        private Guna.UI.WinForms.GunaControlBox btnMinimizar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private Guna.UI.WinForms.GunaButton btnAddTask;
        private Guna.UI.WinForms.GunaLabel gunaLabel2;
        private Guna.UI.WinForms.GunaLabel gunaLabel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Area;
        private System.Windows.Forms.DataGridViewTextBoxColumn Actividad;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaAsignado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comentario;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Realizado;
        private System.Windows.Forms.DataGridViewLinkColumn Modificar;
        private System.Windows.Forms.DataGridViewLinkColumn Eliminar;
    }
}

