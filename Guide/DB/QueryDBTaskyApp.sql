USE TaskyApp
GO
/*====Ejerccios====*/

CREATE TABLE Ejercicios_Piernas(
Id INTEGER NOT NULL PRIMARY KEY IDENTITY(1,1),
Nombre VARCHAR(50) NOT NULL,
Repeticiones INTEGER,
Series INTEGER,
Peso INTEGER,
Descripcion VARCHAR(300)
)

CREATE TABLE Ejercicios_Brazos(
Id INTEGER NOT NULL PRIMARY KEY IDENTITY(1,1),
Nombre VARCHAR(50) NOT NULL,
Repeticiones INTEGER,
Series INTEGER,
Peso INTEGER,
Descripcion VARCHAR(300)
)

CREATE TABLE Ejercicios_Estiramientos(
Id INTEGER NOT NULL PRIMARY KEY IDENTITY(1,1),
Nombre VARCHAR(50) NOT NULL,
Descripcion VARCHAR(300)
)

CREATE TABLE Ejercicios_Abdomen(
Id INTEGER NOT NULL PRIMARY KEY IDENTITY(1,1),
Nombre VARCHAR(50) NOT NULL,
Repeticiones INTEGER,
Series INTEGER,
Peso INTEGER,
Descripcion VARCHAR(300)
)

CREATE TABLE Ejercicios_Skateboarding(
Id INTEGER NOT NULL PRIMARY KEY IDENTITY(1,1),
Nombre VARCHAR(50) NOT NULL,
Descripcion VARCHAR(300)
)
/*=================*/

/*Otras tareas*/

CREATE TABLE TareasMantenimiento(
Id INTEGER NOT NULL PRIMARY KEY IDENTITY(1,1),
Nombre VARCHAR(50) NOT NULL,
Descripcion VARCHAR(300),
Fecha DATETIME NOT NULL
)

CREATE TABLE TareasSociales(
Id INTEGER NOT NULL PRIMARY KEY IDENTITY(1,1),
Nombre VARCHAR(50) NOT NULL,
Descripcion VARCHAR(300),
Fecha DATETIME NOT NULL
)

CREATE TABLE TareasProfesionales(
Id INTEGER NOT NULL PRIMARY KEY IDENTITY(1,1),
Nombre VARCHAR(50) NOT NULL,
Descripcion VARCHAR(300),
Fecha DATETIME NOT NULL
)
/*=========================*/

/*===Todas las �reas===*/
CREATE TABLE Area(
Id INTEGER NOT NULL PRIMARY KEY IDENTITY(1,1),
Nombre VARCHAR(50) NOT NULL,
Fecha DATETIME NOT NULL
)
/*==============*/

/*Todas las tareas de todas las �reas*/
CREATE TABLE AllTasks(
Id INTEGER NOT NULL PRIMARY KEY IDENTITY(1,1),
Id_Area INTEGER FOREIGN KEY REFERENCES Area(Id)
)
/*==============*/

/*Tabla Principal "Tareas"*/
CREATE TABLE Tareas(
Id INTEGER NOT NULL PRIMARY KEY IDENTITY(1,1),
Area VARCHAR(70) NOT NULL,
Id_Tarea INTEGER FOREIGN KEY REFERENCES AllTasks(Id),
Comentario VARCHAR(300),
Realizado BIT,
Fecha DATETIME NOT NULL
)
/*==============*/

/*Drop All
DROP TABLE AllTasks
DROP TABLE Area
DROP TABLE Ejercicios_Piernas
DROP TABLE Ejercicios_Brazos
DROP TABLE Ejercicios_Estiramientos
DROP TABLE Ejercicios_Abdomen
DROP TABLE Ejercicios_Skateboarding
DROP TABLE TareasMantenimiento
DROP TABLE TareasSociales
DROP TABLE TareasProfesionales
DROP TABLE Tareas
*/